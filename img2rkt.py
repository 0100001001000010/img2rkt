#!/usr/bin/env python

# Load an image from a file as a 2D list containing pixel values
import sys
from PIL import Image
image = Image.open(sys.argv[1])
output = open(sys.argv[2], 'w')

output.write(""";; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname test) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #t)))
(require 2htdp/image)
(require 2htdp/universe)

(above
""")


def getRacketColor(pixel):
    return f"(make-color {pixel[0]} {pixel[1]} {pixel[2]})"


for row in range(image.height):
    output.write(' (beside')
    for col in range(image.width):
        output.write(
            f'\n  (rectangle 1 1 "solid" {getRacketColor(image.getpixel((col, row)))})')
    output.write(')')
    if row != image.height - 1:
        output.write('\n\n')

output.write(')\n')
output.close()
